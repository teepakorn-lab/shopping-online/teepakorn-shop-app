const express = require('express');
const app = express();
const port = process.env.PORT || 8083;
const { sequelize } = require('./db/models');

app.get('/', (req, res) => {
    return res.status(200).json({message: "Hello World"})
});

app.listen(port, async () => {
    console.log(`App listening port : ${port}`);
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
});